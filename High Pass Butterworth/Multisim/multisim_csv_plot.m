%{
multisim = csvread('high_pass_butterworth_bode_plotter.csv', 1, 0);

% Create figure
figure1 = figure('Name','Bode plot','Color',[1 1 1]);
axes1 = axes('Parent',figure1);
hold(axes1,'on');

% Create semilogx
semilogx(multisim(:, 1),20*log10(multisim(:, 2)),'Color',[1 0.501960813999176 0]);

xlabel('Frequency (Hz)');
ylabel('Gain (dB)');
ylim(axes1,[-90 12]);
title('Bode plot')
set(axes1,'Color',[0 0 0],'GridAlpha',0.5,'GridColor',[1 1 1],...
    'GridLineStyle',':','MinorGridAlpha',0.5,'MinorGridColor',[1 1 1],'TickDir',...
    'both','TickLength',[0.004 0.0025],'XColor',[0 0 0],'XGrid','on',...
    'XMinorTick','on','XScale','log','YColor',[0 0 0],'YGrid','on');
%}
%=======================================================================================
%{
multisim = csvread('high_pass_butterworth_oscilloscope.csv', 1, 0);

% Create figure
figure1 = figure('Name','Oscilloscope plot','Color',[1 1 1]);
axes1 = axes('Parent',figure1);
hold(axes1,'on');

plot(multisim(:, 1),multisim(:, 2),'Color',[1 0.501960813999176 0]);

xlabel('Time (s)');
ylabel('Voltage (V)');
ylim(axes1,[-15 15]);
xlim(axes1,[-1e-05 0.05]);
title('Oscilloscope plot')
set(axes1,'Color',[0 0 0],'GridAlpha',0.5,'GridColor',[1 1 1],...
    'GridLineStyle',':','MinorGridAlpha',0.5,'MinorGridColor',[1 1 1],'TickDir',...
    'both','TickLength',[0.004 0.0025],'XColor',[0 0 0],'XGrid','on',...
    'XMinorTick','on','YColor',[0 0 0],'YGrid','on');

%=======================================================================================

% Create figure
figure1 = figure('Name','Oscilloscope plot','Color',[1 1 1]);
axes1 = axes('Parent',figure1);
hold(axes1,'on');

plot(multisim(:, 4),multisim(:, 5),'Color',[0 0.5 0]);

xlabel('Time (s)');
ylabel('Voltage (V)');
ylim(axes1,[-15 15]);
xlim(axes1,[-1e-05 0.05]);
title('Oscilloscope plot')
set(axes1,'Color',[0 0 0],'GridAlpha',0.5,'GridColor',[1 1 1],...
    'GridLineStyle',':','MinorGridAlpha',0.5,'MinorGridColor',[1 1 1],'TickDir',...
    'both','TickLength',[0.004 0.0025],'XColor',[0 0 0],'XGrid','on',...
    'XMinorTick','on','YColor',[0 0 0],'YGrid','on');

%=======================================================================================

% Create figure
figure1 = figure('Name','Oscilloscope plot','Color',[1 1 1]);
axes1 = axes('Parent',figure1);
hold(axes1,'on');

plot(multisim(:, 1),multisim(:, 2),'Color',[1 0.501960813999176 0]);
plot(multisim(:, 4),multisim(:, 5),'Color',[0 0.5 0]);

xlabel('Time (s)');
ylabel('Voltage (V)');
ylim(axes1,[-15 15]);
xlim(axes1,[-1e-05 0.05]);
title('Oscilloscope plot')
set(axes1,'Color',[0 0 0],'GridAlpha',0.5,'GridColor',[1 1 1],...
    'GridLineStyle',':','MinorGridAlpha',0.5,'MinorGridColor',[1 1 1],'TickDir',...
    'both','TickLength',[0.004 0.0025],'XColor',[0 0 0],'XGrid','on',...
    'XMinorTick','on','YColor',[0 0 0],'YGrid','on');
%}
%=======================================================================================
%{
multisim = csvread('high_pass_butterworth_spectrum_analyzer_input.csv', 1, 0);

% Create figure
figure1 = figure('Name','Spectrum analyzer plot','Color',[1 1 1]);
axes1 = axes('Parent',figure1);
hold(axes1,'on');

plot(multisim(:, 1),multisim(:, 2),'Color',[1 0.501960813999176 0]);

xlabel('Frequency (Hz)');
ylabel('Amplitude (V)');
ylim(axes1,[0 9]);
xlim(axes1,[0 20000]);
title('Spectrum analyzer plot')
set(axes1,'Color',[0 0 0],'GridAlpha',0.5,'GridColor',[1 1 1],...
    'GridLineStyle',':','MinorGridAlpha',0.5,'MinorGridColor',[1 1 1],'TickDir',...
    'both','TickLength',[0.004 0.0025],'XColor',[0 0 0],'XGrid','on',...
    'XMinorTick','on','YColor',[0 0 0],'YGrid','on');

%=======================================================================================

multisim2 = csvread('high_pass_butterworth_spectrum_analyzer_output.csv', 1, 0);

% Create figure
figure1 = figure('Name','Spectrum analyzer plot','Color',[1 1 1]);
axes1 = axes('Parent',figure1);
hold(axes1,'on');

plot(multisim2(:, 1),multisim2(:, 2),'Color',[0 0.5 0]);

xlabel('Frequency (Hz)');
ylabel('Amplitude (V)');
ylim(axes1,[0 9]);
xlim(axes1,[0 20000]);
title('Spectrum analyzer plot')
set(axes1,'Color',[0 0 0],'GridAlpha',0.5,'GridColor',[1 1 1],...
    'GridLineStyle',':','MinorGridAlpha',0.5,'MinorGridColor',[1 1 1],'TickDir',...
    'both','TickLength',[0.004 0.0025],'XColor',[0 0 0],'XGrid','on',...
    'XMinorTick','on','YColor',[0 0 0],'YGrid','on');

%=======================================================================================

% Create figure
figure1 = figure('Name','Spectrum analyzer plot','Color',[1 1 1]);
axes1 = axes('Parent',figure1);
hold(axes1,'on');

plot(multisim(:, 1),multisim(:, 2),'Color',[1 0.501960813999176 0]);
plot(multisim2(:, 1),multisim2(:, 2),'Color',[0 0.5 0]);

xlabel('Frequency (Hz)');
ylabel('Amplitude (V)');
ylim(axes1,[0 9]);
xlim(axes1,[0 20000]);
title('Spectrum analyzer plot')
set(axes1,'Color',[0 0 0],'GridAlpha',0.5,'GridColor',[1 1 1],...
    'GridLineStyle',':','MinorGridAlpha',0.5,'MinorGridColor',[1 1 1],'TickDir',...
    'both','TickLength',[0.004 0.0025],'XColor',[0 0 0],'XGrid','on',...
    'XMinorTick','on','YColor',[0 0 0],'YGrid','on');
%}